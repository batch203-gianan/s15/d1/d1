console.log("Hello, World!");


// Comments :

// This is an singl line comment

/* 
	This is a 
	multi line comment 
*/

// Syntax and Statements

// Statements in programming are instructions that we tell the computer to perform

// Syntax in programming, it is the set rules that describes how statments must be constructed

// Variables
/*
	- it is used to contain data

	-Syntax in declaring variable
		- let/const variableName;


*/

let myVariable = "Hello";

console.log(myVariable);

/*	Declaring a variable with inital value
	let/const variableName = value;	
*/

let productName = 'desktop computer';
console.log(productName);

let product = "Alvin's Computer"

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

productName = "laptop";
console.log(productName);

let friend = "Kate";
 friend = "Jane";

console.log(friend);

/*interest = "4.489";*/
console.log (interest);

// Reassigning variables vs initializing variables

let supplier;
// Initialization is done after the variables has been declared
supplier = "John Smith Trading"

console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

const pi = 3.1416;
// pi = 3.141516;
console.log(pi);

// Multiple Variable Declaration

let productCode ="DC017";
const productBrand = "Dell";
console.log (productCode, productBrand);

// Using a variable witha a reserved keyword.

// const let = "Hello";

// console.log (let); // error: cannot used reserved name

// [SECTION] Data Types

// Strings - combination
// Strings are sereis of characters that create a keyword, phrase, sentence of anything related to creating text.

let country = 'Philippines';
let province = "Metro Manila";

// concatenating strings
let fullAddress = province + ',' +  country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// Escape character(\)
// \n refers to creating a new line or set the text to next line.
let mailAddress = "Metro Manila\nPhilippines";
console.log (mailAddress);

let message = "John's employees went home early"
console.log(message);

message = 'John\'s employees went home early';
console.log(message);

// Numbers
// Integers/Whole Numbers
let headcount =26;
console.log(headcount);

// Decimal Numbers/Fraction
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combine number and string

console.log("John's grade last quarter is " + grade);

// Boolean
// true/false
let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Array
// it is used to store multiple values with similar data type.
	// let/const arrayName = [elementA, elementB, elementC, .....]
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data tyles
// storing different data tytpes inside an array is not recommended because it will not make sens to in the context of programming.
let details = ["John", "Smith" , 32, true];
console.log(details);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items.
// Syntax

	/*
		let/const objectName = {
			propertyA: value,
			propertyB: value
		}
	*/

	let person = {
		fullName: "Juan Dela Cruz",
		age: 35,
		isMarries: false,
		contact: ["+63917 123 4567", "8123 4567"],
		address: {
			houseNumber: "345",
			city: "Manila"
		}	
	}

	console.log(person);

	// Create abstract object
	let myGrades = {
		firstGrading: 98.7,
		secondGrading: 92.1,
		thirdGrading: 90.2,
		fourthGrading: 94.6
	}
	console.log(myGrades);

	// type of operator is used to determin the type of data or the variable value.
	console.log(typeof myGrades);

	console.log(typeof grades);

	// Constant Objects and Arrays
	// we cannot reassign the value of the variable but we can change the elements of the constant array
	const anime = ["one piece", "one punch man", "attack on titan"];
	anime[0] = "kimetsu no yaiba";

	console.log(anime);

	//Null
	// It is used to intentionally express the absence of the value in a variable/declaration/initialization

	let spouse = null;
	console.log(spouse);

	let myNumber =0; // number
	let myString = ""; //string

	//Undefined
	//represents the state of a variable that has been decalred without an assigned value.

	let fullName
	console.log(fullName);
